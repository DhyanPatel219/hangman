package controller;

import apptemplate.AppTemplate;
import com.fasterxml.jackson.databind.SerializationFeature;
import components.AppDataComponent;
import components.AppFileComponent;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.codehaus.jackson.map.ObjectReader;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ObjectMapper;
import java.io.*;
import java.nio.file.Path;
import javafx.stage.FileChooser;
import java.io.IOException;
import static settings.AppPropertyType.*;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    private static AppTemplate appTemplate; // shared reference to the application
    private GameData gamedata = new GameData(appTemplate); // shared reference to the game being played, loaded or saved
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private boolean     gameover;    // whether or not the current game is already over
    private boolean     savable;
    private Path        workFile;
    private AnimationTimer timer;
    File currentWorkFile;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void start() {
        gameover = false;
        success = false;
        savable = true;
        discovered = 0;

        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        initWordGraphics(guessedLetters);
        play();
    }

    private void end() {
//        System.out.println(success ? "You win!" : "Ah, close but not quite there. The word was \"" + gamedata.getTargetWord() + "\".");
        Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        appTemplate.getGUI().updateWorkspaceToolbar(savable);
        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
//        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);
        remainingGuessBox.getChildren().addAll(new Label(success ? "  You win!" : "  Ah, close but not quite there. The word was \"" + gamedata.getTargetWord() + "\"."));
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameover = true;
        gameButton.setDisable(true);
        savable = false; // cannot save a game that is already over
        appTemplate.getGUI().updateWorkspaceToolbar(savable);

    }

    private void initWordGraphics(HBox guessedLetters) {
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        for (int i = 0; i < progress.length; i++) {
            progress[i] = new Text(Character.toString(targetword[i]));
            progress[i].setVisible(false);
        }
        guessedLetters.getChildren().addAll(progress);
        for (int i = 0; i < progress.length; i++) {
            for (char c : gamedata.getGoodGuesses()) {

                if (gamedata.getTargetWord().charAt(i) == c) {
                    progress[i].setVisible(true);
                    discovered++;

                }
            }
        }
    }

    public void play() {
         timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().toLowerCase().charAt(0);

                    

                    if (!alreadyGuessed(guess)) {
                        boolean goodguess = false;
                        for (int i = 0; i < progress.length; i++) {
                            if (gamedata.getTargetWord().charAt(i) == guess) {
                                progress[i].setVisible(true);
                                gamedata.addGoodGuess(guess);
                                goodguess = true;
                                discovered++;
                            }
                        }
                        if (!goodguess)
                            gamedata.addBadGuess(guess);

                        success = (discovered == progress.length);
                        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                    }
                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }

            @Override
            public void stop() {
                try{
                    super.stop();
                    end();
                } catch (Exception e){

                }
            }
        };
        timer.start();
    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }
    
    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (savable)
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            gamedata = new GameData(appTemplate);
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file

            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

        if (gameover) {
            savable = false;
            appTemplate.getGUI().updateWorkspaceToolbar(savable);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
            enableGameButton();
        }

    }
    @Override
    public void handleSaveRequest() throws IOException {

        //String java = new String("Error");
        // WE'LL NEED THIS TO GET CUSTOM STUFF
        //PropertiesManager props = PropertiesManager.getPropertiesManager();

        PropertyManager props = PropertyManager.getManager();
        try {

            if (currentWorkFile != null) {
                saveWork(currentWorkFile);
            } else {
                FileChooser fc = new FileChooser();
                fc.setInitialDirectory
                        (new File(System.getProperty("user.home")));
                fc.setTitle(props.getPropertyValue(SAVE_WORK_TITLE));
                fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(props.getPropertyValue(WORK_FILE_EXT_DESC),
                        props.getPropertyValue(WORK_FILE_EXT)));
                File selectedFile = fc.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null) {
                    saveWork(selectedFile);

                }
            }
        } catch (IOException ioe){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }
//        try {
//            // MAYBE WE ALREADY KNOW THE FILE
//
//            // PROMPT THE USER FOR A FILE NAME
//            FileChooser fc = new FileChooser();
//            fc.setInitialDirectory(new File(System.getProperty("user.home")));
//            fc.setTitle("hangman");
//            //fc.getExtensionFilters().addAll(
//            //new ExtensionFilter(props.getProperty(WORK_FILE_EXT_DESC), props.getProperty(WORK_FILE_EXT)));
//
//            File selectedFile = fc.showSaveDialog(appTemplate.getGUI().getWindow());
//            if (selectedFile != null) {
//                saveWork(selectedFile);
//            }
//
//            } catch(Exception ioe){
//                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//                dialog.show("error","error");
//            }
        }



    public void saveWork(File selectedFile) throws IOException
    {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), (selectedFile.toPath()));

//        Path pth = selectedFile.toPath();
//        String path = (pth.toString() + ".json");

        currentWorkFile = selectedFile;
        savable = true;

        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager props = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));

        appTemplate.getGUI().updateWorkspaceToolbar(savable);

        FileOutputStream is = new FileOutputStream(selectedFile);
        OutputStreamWriter osw = new OutputStreamWriter(is);
        Writer w = new BufferedWriter(osw);
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
// ow.writeValue(new File(workFile.toString()), gamedata.getClass());
//        String json = ow.writeValueAsString(gamedata);
        AppDataComponent dataManager = appTemplate.getDataComponent();
        String json = ow.writeValueAsString(gamedata);
        w.write(json);
        w.close();

//        ObjectMapper mapp = new ObjectMapper();
//        mapp.writeValue(new File(path), gamedata);
//        String jsonInString = mapp.writeValueAsString(gamedata);
////        mapp.configure(SerializationFeature.INDENT_OUTPUT, true);
//        FileWriter w = new FileWriter(path);
//        w.write(jsonInString);
//        w.close();

//        handleNewRequest();
    }
    @Override
    public void handleLoadRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (savable) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A Course
            if (continueToOpen) {
                // GO AHEAD AND PROCEED LOADING A Course
                promptToOpen();
                //
                timer.stop();
                start();
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager props = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), props.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
        }
    }
//        try {
//            // WE MAY HAVE TO SAVE CURRENT WORK
//            boolean continueToOpen = true;
//            if (!savable) {
//                // THE USER CAN OPT OUT HERE WITH A CANCEL
//                continueToOpen = promptToSave();
//            }
//
//            // IF THE USER REALLY WANTS TO OPEN A Course
//            if (continueToOpen) {
//                // GO AHEAD AND PROCEED LOADING A Course
//                promptToOpen();
//            }
//        } catch (IOException ioe) {
////            // SOMETHING WENT WRONG
//            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
//            PropertyManager props = PropertyManager.getManager();
//            dialog.show(props.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE), props.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
//        }
    
    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (savable)
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }
    
    private boolean promptToSave() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        String selection = yesNoCancelDialog.getSelection();

        if (selection.equals(YesNoCancelDialogSingleton.YES)) {

//            AppDataComponent dm = appTemplate.getDataComponent();

            if (currentWorkFile == null) {

                FileChooser fc = new FileChooser();
               // fc.setInitialDirectory(new File("user.home"));
                fc.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
                fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(propertyManager
                .getPropertyValue(WORK_FILE_EXT_DESC), propertyManager.getPropertyValue(WORK_FILE_EXT)));

                File selectedFile = fc.showSaveDialog(appTemplate.getGUI().getWindow());
                if (selectedFile != null) {
                    saveWork(selectedFile);
                    savable = true;
                    }
                }
                else {
                    saveWork(currentWorkFile);
                    savable = true;
                }
            } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
            // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
            else if (selection.equals(YesNoCancelDialogSingleton.CANCEL)) {
                return false;
            }

            // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
            // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
            // HAD IN MIND IN THE FIRST PLACE
                return true;
            }


    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {

    }

    public void promptToOpen(){


        // WE'LL NEED TO GET CUSTOMIZED STUFF WITH THIS
        PropertyManager props = PropertyManager.getManager();

        // AND NOW ASK THE USER FOR THE FILE TO OPEN
        FileChooser fc = new FileChooser();

        //fc.setInitialDirectory(new File("user.home"));
        fc.setTitle(props.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE));
        File selectedFile = fc.showOpenDialog(appTemplate.getGUI().getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
                AppDataComponent dataManager = appTemplate.getDataComponent();

                AppFileComponent fileManager = appTemplate.getFileComponent();
                fileManager.loadData(dataManager, selectedFile.toPath());

                ObjectMapper mapper = new ObjectMapper();

                gamedata = mapper.readValue(new File(selectedFile.getPath()), GameData.class);
                appTemplate.getWorkspaceComponent().reloadWorkspace();
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
                // MAKE SURE THE WORKSPACE IS ACTIVATED
                appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
                savable = true;
                appTemplate.getGUI().updateWorkspaceToolbar(savable);
            } catch (Exception e) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getPropertyValue(PROPERTIES_LOAD_ERROR_TITLE),
                        props.getPropertyValue(PROPERTIES_LOAD_ERROR_MESSAGE));
            }
        }
    }

}
